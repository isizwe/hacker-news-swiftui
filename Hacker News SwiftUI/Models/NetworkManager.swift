import Foundation

class NetworkManager: ObservableObject {
    @Published var posts = [Post]()
    
    func fetchData() {
        guard let url = URL(string: "https://hn.algolia.com/api/v1/search?tags=front_page") else { return }
        
        URLSession(configuration: .default).dataTask(with: url) { data, response, error in
            if error != nil { return }
            guard let safeData = data else { return }
            do {
                let results = try JSONDecoder().decode(Results.self, from: safeData)
                DispatchQueue.main.async {
                    self.posts = results.hits
                }
            } catch {
                print(error)
            }
        }.resume()
    }
}
